class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.attachment :image
      t.text :address
      t.text :description
      t.datetime :occur_at
      t.boolean :active
      t.integer :views
      t.string :slug

      t.timestamps
    end
    add_index :events, :slug, unique: true
  end
end
