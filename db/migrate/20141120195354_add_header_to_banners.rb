class AddHeaderToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :header, :boolean
  end
end
