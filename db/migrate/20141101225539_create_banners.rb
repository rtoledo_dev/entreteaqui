class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :title
      t.attachment :image
      t.string :url
      t.boolean :slider
      t.boolean :top
      t.boolean :top_small
      t.boolean :active

      t.timestamps
    end
  end
end
