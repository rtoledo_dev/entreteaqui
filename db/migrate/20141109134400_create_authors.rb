class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :name
      t.text :about
      t.string :slug

      t.timestamps
    end
  end
end
