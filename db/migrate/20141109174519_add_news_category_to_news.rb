class AddNewsCategoryToNews < ActiveRecord::Migration
  def change
    add_reference :news, :news_category, index: true
  end
end
