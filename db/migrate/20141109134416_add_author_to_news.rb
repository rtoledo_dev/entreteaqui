class AddAuthorToNews < ActiveRecord::Migration
  def change
    change_table :news do |t|
      t.references :author, index: true
    end
  end
end
