class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.attachment :image
      t.text :description
      t.datetime :occur_at
      t.boolean :active
      t.integer :views
      t.string :slug

      t.timestamps
    end
    add_index :news, :slug, unique: true
  end
end
