class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :title
      t.string :singer
      t.string :youtube_code
      t.boolean :active

      t.timestamps
    end
  end
end
