class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.references :company_category, index: true
      t.string :name
      t.attachment :logo
      t.text :description
      t.boolean :active

      t.timestamps
    end
  end
end
