class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.references :event, index: true
      t.string :title
      t.attachment :image
      t.text :description
      t.text :address
      t.datetime :occur_at
      t.boolean :active
      t.integer :views
      t.string :slug

      t.timestamps
    end
    add_index :galleries, :slug, unique: true
  end
end
