class GalleryImage < ActiveRecord::Base
  belongs_to :gallery

  validates :gallery_id, presence: true

  has_attached_file :image, styles: {thumb: '210x130#', big: '1024'}, default_style: :thumb,convert_options: {thumb: "-quality 90 -strip", big: "-quality 90 -strip"}
  do_not_validate_attachment_file_type :image
  # validates_attachment_presence :image
end
