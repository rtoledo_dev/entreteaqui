class Company < ActiveRecord::Base
  include RotaConnection
  belongs_to :company_category
  has_many :company_categorizations
  has_many :company_categories, through: :company_categorizations

  extend FriendlyId
  friendly_id :name, use: :slugged

  has_attached_file :photo0, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo1, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo2, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo3, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo4, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :photo5, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  has_attached_file :logo, styles: {thumb: '76x76#', big: '600', box: '215x215#'}
  do_not_validate_attachment_file_type :photo0
  do_not_validate_attachment_file_type :photo1
  do_not_validate_attachment_file_type :photo2
  do_not_validate_attachment_file_type :photo3
  do_not_validate_attachment_file_type :photo4
  do_not_validate_attachment_file_type :photo5
  do_not_validate_attachment_file_type :logo

  def self.active
    where(active: true).where("? BETWEEN companies.start_at AND companies.end_at", Date.today)
  end
end
