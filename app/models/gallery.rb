class Gallery < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  has_attached_file :image, styles: {thumb: '64x57#', thumb_top: '67x54#', head: '670', big: '1024'}, default_style: :thumb,convert_options: {head: "-quality 90 -strip", big: "-quality 90 -strip"}
  attr_accessor :image_directories

  belongs_to :event
  has_many :gallery_images

  validates :title, :description, :address, presence: true
  validates_attachment_presence :image

  # do_not_validate_attachment_file_type :image

  def self.active
    where(active: true)
  end

  def self.sorted
    order(occur_at: :desc)
  end

  def self.uncompress(id)

  end


  def save_images_in_folder
    unless self.image_directories.blank?
      file_pattern = File.join(Rails.root,'fotos',self.image_directories,"*.{jpg,jpeg,JPG,JPEG}")
      files = Dir.glob(file_pattern)
      files.each do |f_path|
        file_moved = "#{self.to_param[0,10]}-#{Time.now.to_i}-#{File.basename(f_path)}"
        file_moved_name = File.join(Rails.root,'fotos',self.image_directories,file_moved)
        FileUtils.mv(f_path,file_moved_name)
        GalleryImage.transaction do
          begin
            gallery_image = self.gallery_images.build(image: File.open(file_moved_name))
            gallery_image.save!
            File.unlink(file_moved_name)
          rescue => e
            raise e.message
            FileUtils.mv(file_moved_name,f_path)
          end
        end
      end
      FileUtils.rmdir(File.join(Rails.root,'fotos',self.image_directories))
    end
    true
  end
end
