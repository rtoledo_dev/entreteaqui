class NewsCategory < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  validates :name, presence: true

  def self.sorted
    order(name: :desc)
  end
end
