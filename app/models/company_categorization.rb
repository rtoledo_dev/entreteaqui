class CompanyCategorization < ActiveRecord::Base
  include RotaConnection
  belongs_to :company
  belongs_to :company_category
end
