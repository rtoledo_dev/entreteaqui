module RotaConnection
  extend ActiveSupport::Concern
  included do
    establish_connection "#{Rails.env}_rotaempresarial"
  end
end
