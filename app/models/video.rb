class Video < ActiveRecord::Base
  validates :title, :singer, :youtube_code, presence: true

  def self.active
    where(active: true)
  end

  def self.sorted
    order(created_at: :desc)
  end
end
