json.array!(@banners) do |banner|
  json.extract! banner, :id, :title, :image, :url, :slider, :top, :top_small, :active
  json.url banner_url(banner, format: :json)
end
