json.array!(@events) do |event|
  json.extract! event, :id, :title, :image, :address, :description, :occur_at, :active
  json.url event_url(event, format: :json)
end
