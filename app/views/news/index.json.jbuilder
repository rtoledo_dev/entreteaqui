json.array!(@news) do |news|
  json.extract! news, :id, :title, :image, :description, :occur_at, :active, :views, :slug
  json.url news_url(news, format: :json)
end
