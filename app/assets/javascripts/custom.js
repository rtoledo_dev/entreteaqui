jQuery(document).on("page:load ready", function(){

	// ---------------------------------------------------------
	// Tabs
	// ---------------------------------------------------------
	jQuery(".tabs").each(function(){

		jQuery(this).find(".tab").hide();
		jQuery(this).find(".tab-menu li:first a").addClass("active").show();
		jQuery(this).find(".tab:first").show();

	});

	jQuery(".tabs").each(function(){

		jQuery(this).find(".tab-menu a").click(function() {

			jQuery(this).parent().parent().find("a").removeClass("active");
			jQuery(this).addClass("active");
			jQuery(this).parent().parent().parent().parent().find(".tab").hide();
			var activeTab = jQuery(this).attr("href");
			jQuery(activeTab).fadeIn();
			return false;

		});

	});


	// ---------------------------------------------------------
	// Toggle
	// ---------------------------------------------------------

	jQuery(".toggle").each(function(){

		jQuery(this).find(".box").hide();

	});

	jQuery(".toggle").each(function(){

		jQuery(this).find(".trigger").click(function() {

			jQuery(this).toggleClass("active").next().stop(true, true).slideToggle("normal");

			return false;

		});

	});

	jQuery(".top-star-news .latestpost li:odd").addClass("odd");
	jQuery(".top-star-news .latestpost li:even").addClass("even");

	/* Home Tab List */
	jQuery(".tab ul li:even").addClass("even");
	jQuery(".tab ul li:odd").addClass("odd");

	/* Latest News Carousel List */
	jQuery("#carousel .overview li:even").addClass("even");

	/* Sound List */
	jQuery(".top-sound-box .latestpost li:odd").addClass("odd");

	/* Footer color list */
	jQuery("#widget-footer > div:nth-child(2n)").addClass("second");
	jQuery("#widget-footer > div:nth-child(3n)").addClass("third");
	jQuery("#widget-footer > div:nth-child(4n)").addClass("fourth");



	// ---------------------------------------------------------
	// Social Icons
	// ---------------------------------------------------------

	jQuery(".social-networks li a").tooltip({ effect: 'slide', position: 'bottom center', opacity: .5 });


	jQuery('.banners-holder a')
	.hover(function(){
		jQuery(this).stop().animate({textIndent:15}, {duration:300})
	}, function(){
		jQuery(this).stop().animate({textIndent:0}, {duration:300})
	});

});
