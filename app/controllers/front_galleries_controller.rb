class FrontGalleriesController < ApplicationController
  def index
    @galleries = Gallery.active.sorted
  end

  def show
    @gallery = Gallery.friendly.find(params[:id])
  end
end
