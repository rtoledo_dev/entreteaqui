class GalleriesController < AdminController
  before_action :set_gallery, only: [:show, :edit, :update, :destroy]

  def index
    @galleries = Gallery.all
  end

  def show
  end

  def new
    @gallery = Gallery.new
  end

  def edit
  end

  def create
    @gallery = Gallery.new(gallery_params)
    respond_to do |format|
      if @gallery.save
        @gallery.save_images_in_folder
        format.html { redirect_to galleries_url, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :created, location: @gallery }
      else
        format.html { render :new }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @gallery.update(gallery_params)
        @gallery.save_images_in_folder
        format.html { redirect_to galleries_url, notice: 'Operação realizada com sucesso.' }
        format.json { render :show, status: :ok, location: @gallery }
      else
        format.html { render :edit }
        format.json { render json: @gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @gallery.destroy
    respond_to do |format|
      format.html { redirect_to galleries_url, notice: 'Registro apagado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    def set_gallery
      @gallery = Gallery.friendly.find(params[:id])
    end

    def gallery_params
      params.require(:gallery).permit(:event_id, :title, :image, :description,
        :address, :occur_at, :active, :views, :slug, :image_directories)
    end
end
