class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :footer_last_galleries, :footer_last_events, :in_admin?,
    :header_banner

  def footer_last_galleries
    @footer_last_galleries ||= Gallery.active.sorted.limit(5)
  end

  def footer_last_events
    @footer_last_events ||= Event.active.sorted.limit(5)
  end

  def header_banner
    @header_banner ||= Banner.active.where(header: true).first
  end

  def in_admin?
    defined?(@in_admin) && @in_admin
  end
end
