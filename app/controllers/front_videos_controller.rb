class FrontVideosController < ApplicationController
  def index
    @videos = Video.active.sorted
  end
end
