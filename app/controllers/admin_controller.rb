class AdminController < ApplicationController
  before_action :authenticate_user!, :set_admin

  def set_admin
    @in_admin = true
  end
end
