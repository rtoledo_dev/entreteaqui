class FrontEventsController < ApplicationController
  def index
    @events = Event.active.sorted
  end

  def show
    @event = Event.friendly.find(params[:id])
  end
end
