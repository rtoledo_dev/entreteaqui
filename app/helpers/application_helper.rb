module ApplicationHelper

  def title(title)
    content_for(:title) do
      title
    end
    title
  end

  def admin_title(title)
    title("ÁREA ADMINISTRATIVA - #{title}")
  end

  def flash_messages
    html = []
    flash.each do |k,v|
      new_key = k

      if k.to_s == "alert"
        new_key = :danger
      elsif k.to_s == "notice"
        new_key = :success
      end
      html << content_tag(:div, v.html_safe, class: "alert alert-#{new_key}")
    end
    html.join.html_safe
  end

  def current_menu_css_class(path)
    css_class = "current-menu-item page_item current_page_item"
    if current_page?(root_path)
      if path == "/"
        css_class
      end
    else
      paths = path.is_a?(Array) ? path : [path]
      paths.reject!{|t| t == "/"}
      paths.each do |path|
        return css_class if request.path.include?(path)
      end
    end
  end

  def front_company_path(company)
    "http://www.rotaempresarial.com.br/empresa/#{company.slug}"
  end

  def default_host_to_email
    Rails.env == "development" ? 'http://localhost:3000' : 'http://entreteaqui.rtoledo.inf.br'
  end
end
