Rails.application.routes.draw do
  resources :news_categories
  resources :authors
  resources :gallery_images
  resources :videos
  resources :news
  resources :galleries
  resources :events
  resources :banners

  resources :front_galleries, path: 'fotos'
  resources :front_events, path: 'agenda'
  resources :front_videos, path: 'videos-web'
  resources :front_news, path: 'colunas'
  resources :front_companies, path: 'guia-empresarial'

  match "colunas/por-colunista/:author_id", :to => "front_news#by_author", :as => :news_by_author, via: :get

  get 'fale-conosco' => 'front_talk_with_us#index', as: :front_talk_with_us
  post 'fale-conosco/enviar' => 'front_talk_with_us#deliver', as: :send_contact

  devise_for :users
  get 'home/index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
